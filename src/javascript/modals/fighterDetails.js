import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name} = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const propertiesElement = createFighterProperties(fighter);

  nameElement.innerText = name;
  fighterDetails.append(nameElement, propertiesElement);

  return fighterDetails;
}

function createFighterProperties(fighter) {
  const { attack, defense, health, source} = fighter;

  const fighterProperties = createElement({ tagName: 'div', className: 'fighter-properties' });
  const attackElement = createElement({ tagName: 'div', className: 'fighter-attack' });
  const defenseElement = createElement({ tagName: 'div', className: 'fighter-defense' });
  const healthElement = createElement({ tagName: 'div', className: 'fighter-health' });

  const attributes = { src: source};
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });
  attackElement.innerText  = 'Attack: ' + attack;
  defenseElement.innerText = 'Defense: ' + defense;
  healthElement.innerText = 'Health: ' + health;
  fighterProperties.append(attackElement, defenseElement, healthElement, imgElement);

  return fighterProperties;
}
