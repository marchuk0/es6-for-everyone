import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showWinnerModal(fighter) {
    const title = 'Winner is ' + fighter.name + '!';
    const bodyElement = createWinnerDetails(fighter);
    showModal({ title, bodyElement});
}

function createWinnerDetails(fighter) {
    const { source } = fighter;

    const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
    const attributes = { src: source};
    const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });

    fighterDetails.append(imgElement);
    return fighterDetails;
}