
export function fight(firstFighter, secondFighter) {
    let firstHealth = firstFighter.health;
    let secondHealth = secondFighter.health;
    while(firstHealth > 0) {
        let damage = getDamage(firstFighter, secondFighter);
        secondHealth -= damage;
        [firstHealth , secondHealth] = [secondHealth, firstHealth];
        [firstFighter, secondFighter] = [secondFighter, firstFighter];
    }
    return secondFighter;
}


export function getDamage(attacker, enemy) {
    return Math.max(getHitPower(attacker) - getBlockPower(enemy), 0);
}

export function getHitPower(fighter) {
    return fighter.attack * getRandom(1, 2);
}

export function getBlockPower(fighter) {
    return fighter.defense * getRandom(1, 2);
}

function getRandom(min, max) {
    return min + Math.random() * (max - min);
}
